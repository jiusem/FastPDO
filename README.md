#### FastPDO
简化PDO操作，注重简单，安全。
#### 配置
后期会支持动态配置，便于切换数据库 
```php
<?php
private $config = array(
	'host' => '192.168.24.155',
	'user' => 'crazymus',
	'pass' => '123',
	'charset' => 'utf-8',
	'db_name' => 'notebook'
);
?>
```
####使用
实例化 :
```php
<?php
$pdo = FastPDO::getInstance();
?>
```
为防止SQL注入，所有SQL语句采用参数绑定形式。
获取单行记录：
```php
<?php
$sql = "select * from list where id = ? and cate = ? ";
$params = array(1, 'default');
$data = $pdo->getOne($sql, $params);
?>
```
获取多行记录：
```php
<?php
$sql = "select * from list where id = ? and cate = ? ";
$params = array(1, 'default');
$data = $pdo->getSome($sql, $params);
?>
```
写入数据：
```php
<?php
$sql = "insert into list values (?, ?, ?)";
$params = array('name', 'sex', 'age');
//成功返回新插入的记录ID，失败返回NULL
$insert_id = $pdo->insert($sql, $params);
?>
```
更新、删除数据：
```php
<?php
$sql = "update list set name = ? where id = ? ";
$params = array('name', 1);
//成功返回受影响行数 失败返回false
$affected_rows = $pdo->query($sql, $params);
?>
```
模糊查询：
```php
<?php
$result = $pdo->getSome(" select * from cates where cate_name like ? ", array('%分类%'));
print_r($result);
?>
```
事务支持:
```php
<?php
//开始事务 
$pdo->beginTransaction();
//提交事务
$pdo->commit();
//回滚事务
$pdo->rollBack();
?>
```